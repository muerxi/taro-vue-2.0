interface SwiperItemInterface {
    imgUrl: string;
    pageUrl: string;
}

interface MenuItemInterface {
    iconUrl: string;
    text: string;
    pageUrl: string;
}

interface PostArticleInterface {
    headImgUrl: string;
    postId: number;
    articleType: number | 0 | 1;
    createTime: string;
    author: string;
    title: string;
    content?: string;
    articleUrl?: string;
}
