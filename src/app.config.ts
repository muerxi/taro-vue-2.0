export default defineAppConfig({
  "__usePrivacyCheck__": true,
  pages: [
    'pages/index/index',
    'pages/login/index',
    'pages/appointment/index',
    'pages/add-appointment/index',
    'pages/address/index',
    'pages/water-use-detail/index',
    'pages/recharge/index',
    'pages/service/index',
    'pages/payment-detail/index',
    'pages/account/index',
    'pages/search-customer/index',
    'pages/account-choose/index',
    'pages/water-quality/index',
    'pages/page-view/index',
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  },
  usingComponents: {
    'ec-canvas': 'components/ec-canvas2/ec-canvas'
  }
})
