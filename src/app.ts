import {createApp} from 'vue'
// 定制化主题必须使用 scss
import '@nutui/nutui-taro/dist/styles/themes/default.scss';
//taro
import {
  Tabs,
  TabPane,
  Toast,
  Button,
  Cell,
  Tabbar,
  TabbarItem,
  Icon,
  Layout,
  Row,
  Col,
  Popup,
  OverLay,
  Input,
  Collapse,
  Dialog,
  SearchBar,
  Drag,
  FixedNav,
  CollapseItem,
  ActionSheet,
  InfiniteLoading,
  Grid, GridItem, Address,Elevator
} from '@nutui/nutui-taro';
import './app.scss'


const App = createApp({
  // onShow(options: any) {
  //     console.log(options);
  // },
  // 入口组件不需要实现 render 方法，即使实现了也会被 taro 所覆盖
})

App.use(Button)
    .use(InfiniteLoading)
    .use(Toast)
    .use(Popup)
    .use(Drag)
    .use(Dialog)
    .use(FixedNav)
    .use(ActionSheet)
    .use(Grid)
    .use(SearchBar)
    .use(GridItem)
    .use(Tabs)
    .use(TabPane)
    .use(Address)
    .use(Elevator)
    .use(Collapse)
    .use(CollapseItem)
    .use(Input)
    .use(Cell)
    .use(OverLay)
    .use(Tabbar)
    .use(TabbarItem)
    .use(Layout)
    .use(Row)
    .use(Col)
    .use(Icon);
export default App
