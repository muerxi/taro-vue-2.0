import Taro from '@tarojs/taro';

export interface WeChatUserInfo {
    avatarUrl: string;
    nickName: string;

    [key: string]: any;
}

/**
 * 保存用户基本信息
 * @param res
 */
export function saveUserInfo(res: WeChatUserInfo) {
    return Taro.setStorage({key: 'userInfo', data: res});
}

/**
 * 获取用户基本信息
 */
export function getUserInfo(): Promise<WeChatUserInfo> {
    return new Promise<WeChatUserInfo>((resolve, reject) => {
        Taro.getStorage({key: 'userInfo'}).then(res => {
            resolve(res.data);
        }).catch(error => {
            reject(error);
        })
    });
}


/**
 * 保存用户openId
 * @param openId
 */
export function saveOpenId(openId: string) {
    return Taro.setStorage({key: '_openId', data: openId});
}

/**
 * 获取用户openId
 */
export function getOpenId(): Promise<string> {
    return new Promise<string>((resolve, reject) => {
        Taro.getStorage({key: '_openId'}).then(res => {
            resolve(res.data);
        }).catch(error => {
            reject(error);
        })
    });
}
