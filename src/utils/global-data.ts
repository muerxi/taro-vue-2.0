const globalData = {}

/**
 * 设置全局变量
 * @param key
 * @param val
 */
export function setGlobalValue(key, val) {
    globalData[key] = val
}

/**
 * 获取全局变量
 * @param key
 */
export function getGlobalValue(key) {
    return globalData[key]
}
