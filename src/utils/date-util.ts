/**
 * 获取本月的日期数组
 */
export function makeThisMonthDays(year: number, month: number): string[] {
    const days = new Date(year, month, 0).getDate();
    let daysArray = [] as any;
    for (let i = 1; i <= days; i++) {
        const monthStr = month < 10 ? "0" + month.toString() : month.toString();
        const dayStr = i < 10 ? "0" + i.toString() : i.toString();
        const dateStr = year.toString() + '-' + monthStr + '-' + dayStr;
        daysArray.push(dateStr);
    }
    return daysArray;
}

export function getLast12Months() {
    let today = new Date();
    today.setMonth(today.getMonth(), 1);
    const todayMonthStr = getDateStr(today) + ' 23:59:59';

    let last12Month = new Date();
    last12Month.setMonth(last12Month.getMonth() - 12, 0);
    const last12MonthStr = getDateStr(last12Month) + ' 23:59:59';
    return [todayMonthStr, last12MonthStr];

}

function getDateStr(date) {
    const todayMonth = date.getMonth() + 1;
    const year = date.getFullYear();
    const days = new Date(year, todayMonth, 0).getDate();
    const monthStr = todayMonth < 10 ? "0" + todayMonth.toString() : todayMonth.toString();
    const dayStr = days < 10 ? "0" + days.toString() : days.toString();
    return year.toString() + '-' + monthStr + '-' + dayStr;
}
