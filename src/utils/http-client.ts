import Taro from '@tarojs/taro';
import {makeSign} from './sign';

// const apiHost = "http://127.0.0.1:8082";
const apiHost = "https://haoshuida-api.muerxi.com";

// 创建请求头部信息
const makeHeader = (url: string, data: any, contentType: string) => {
    const _contentType = {'content-type': contentType};
    const _authorization = {'Authorization': makeSign(url, data),};
    return {..._contentType, ..._authorization};
}

Taro.addInterceptor(Taro.interceptors.logInterceptor);
Taro.addInterceptor(Taro.interceptors.timeoutInterceptor);

/**
 * 网络请求库插件
 */
export default {
    baseRequest(option: Taro.request.Option, contentType: string = 'application/json'): Promise<any> {
        option.header = makeHeader(option.url, option.data, contentType);
        option.url = apiHost + option.url;
        return new Promise((resolve, reject) => {
            Taro.request(option).then(res => {
                if (res.data.code === '0000') {
                    resolve(res.data)
                } else {
                    Taro.showToast({
                        title: res.data.message,
                        icon: 'none',
                        duration: 2000
                    }).then(() => {
                        reject(res.data);
                    });
                }
            }).catch(e => reject(e)).finally(() => {
                console.log(option.url + ' ==>本次请求结束');
            });
        });
    },

    /**
     * get请求
     * @param url
     * @param data
     * @returns
     */
    get(url: string, data: any): Promise<any> {
        let params: Taro.request.Option = {url, data, method: 'GET'}
        return this.baseRequest(params);
    },

    /**
     * post请求
     * @param url
     * @param data
     * @param contentType
     * @returns
     */
    post(url: string, data: any, contentType: string = 'application/json'): Promise<any> {
        let params: Taro.request.Option = {url, data, method: 'POST'}
        return this.baseRequest(params, contentType);
    },

    /**
     * put请求
     * @param url
     * @param data
     * @param contentType
     * @returns
     */
    put(url: string, data: any, contentType: string = 'application/json'): Promise<any> {
        let params: Taro.request.Option = {url, data, method: 'PUT'}
        return this.baseRequest(params, contentType);
    },

    /**
     * delete请求
     * @param url
     * @param data
     * @returns
     */
    delete(url: string, data: any): Promise<any> {
        let params: Taro.request.Option = {url, data, method: 'DELETE'}
        return this.baseRequest(params);
    },
}
