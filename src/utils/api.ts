// 接口请求
import HttpUtil from './http-client';


/**
 * 获取openid
 * @param code
 */
export function getWechatOpenId(code: string): Promise<any> {
    return HttpUtil.post('/wechat/miniAppAccounts/getUserInfo', {code: code});
}

/**
 * 获取小区数据
 * @param id
 */
export function getAreaTree(id: number): Promise<any> {
    return HttpUtil.post('/appointment/address/getAllAreaTreeList', {id: id});
}

/**
 * 获取单元楼栋数据
 * @param id
 */
export function getUnitTree(id: number): Promise<any> {
    return HttpUtil.post('/appointment/address/getBuildings', {id: id});
}

/**
 * 新增预约
 * @param obj
 */
export function addAppointment(obj: any): Promise<any> {
    return HttpUtil.post('/appointment/wechat/add', obj);
}

/**
 * 根据openId，获取所有预约列表
 * @param openId
 */
export function getAppointment(openId: string) {
    return HttpUtil.post('/appointment/wechat/list', {openId: openId});
}

/**
 * 根据openId，获取所有绑定信息
 * @param openId
 */
export function getWechatBindList(openId: string) {
    return HttpUtil.post('/miniApp/cusWx/getBindList', {openId: openId});
}

/**
 * 根据openId，customerId删除关联关系
 * @param openId
 * @param customerId
 */
export function delWechatBind(openId: string, customerId: number) {
    return HttpUtil.post('/miniApp/cusWx/delete', {openId: openId, customerId: customerId, bindType: 1});
}


/**
 * 根据customerId获取充值记录
 * @param customerId
 * @param pageNo
 * @param pageSize
 */
export function getRechargeList(customerId: number, pageNo: number, pageSize: number) {
    return HttpUtil.post('/miniApp/cusWx/rechargeList', {
        pageNo: pageNo,
        pageSize: pageSize,
        queryFilter: {customerId: customerId}
    });
}

/**
 * 根据customerId获取月度消费记录
 * @param customerId
 * @param beginDate
 * @param endDate
 */
export function getMouthMeterVolume(customerId: number, beginDate: string, endDate: string) {
    return HttpUtil.post('/miniApp/cusWx/mouthMeterVolume', {
        customerId: customerId,
        beginDate: beginDate,
        endDate: endDate
    });
}

/**
 * 根据水表编号获取用户信息
 * @param meterNo
 * @param pageNo
 * @param pageSize
 */
export function searchCustomerInfo(meterNo: string) {
    return HttpUtil.post('/miniApp/cusWx/searchCustomerInfo', {
        pageNo: 1,
        pageSize: 10,
        queryFilter: {meterNo: meterNo, meterBizType: 8}
    });
}

/**
 * 绑定微信openid与用户的关系
 * @param customerId
 * @param openid
 */
export function bindCustomerInfo(customerId: number, openid: string) {
    return HttpUtil.post('/miniApp/cusWx/bind', {
        customerId: customerId,
        openid: openid,
        bindType: 1
    });
}

/**
 * 检查表具是否能够充值
 * @param meterId
 * @param meterNo
 */
export function aepMeterCanReCharge(meterId: number, meterNo: string) {
    return HttpUtil.post('/miniApp/cusWx/aepMeterCanReCharge', {
        meterId: meterId,
        meterNo: meterNo
    });
}

/**
 * 微信小程序下单
 * @param req
 */
export function miniAppOrder(req: any) {
    return HttpUtil.post('/pay/wxMiniPay/miniAppOrder', req);
}
