// 接口签名工具
import CryptoJS from 'crypto-js/crypto-js'
import {formatDate} from "./date";
import jsonData from '../assets/config.json';

// TODO: 正式部署的时候，需要更换公钥和私钥
// 接口签名私钥
export const privateKey = jsonData.privateKey;
// TODO：正式部署时，根据不用的应用进行切换
// 小程序Id
export const appId = jsonData.appId;


export function makeSign(urlSuffix: string, body: any) {

    const timeStr = formatDate('yyyy-MM-dd hh:mm:ss',new Date());
    const nonceStr = randomString(16);
    const bodyStr = JSON.stringify(body);
    const list = [appId, urlSuffix, timeStr, nonceStr, bodyStr];
    const signMessage = buildSignMessage(list);
    let signature = CryptoJS.HmacSHA256(signMessage, privateKey).toString();
    const signParams = {
        appId: appId,
        nonceStr: nonceStr,
        signature: signature.toUpperCase(),
        timestamp: timeStr,
        urlSuffix: urlSuffix
    }
    return makeAuthStr(signParams);
}

/**
 * 构建签名字符串
 * @param list
 */
function buildSignMessage(list: string[]) {
    let str = "";
    list.forEach(item => {
        str = str + item + "\n";
    });
    return str;
}

/**
 * 生成签名内容
 * @param params
 */
function makeAuthStr(params: any) {
    const keys = Object.keys(params);
    let str = "MUERXISIGNV4-SHA256-RSA2048 ";
    let strArr = keys.map(key => {
        return key + `="` + params[key] + `"`;
    });
    return str + strArr.join(',');
}

/**
 * 生成长度限定的随机数
 * @param length
 */
function randomString(length) {
    const chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}
