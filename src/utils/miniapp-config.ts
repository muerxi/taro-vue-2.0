import Taro from "@tarojs/taro";

const configApiHost = "https://haoshuida-miniapp.muerxi.com";


function baseRequest(option: Taro.request.Option): Promise<any> {
    option.url = configApiHost + option.url;
    return new Promise((resolve, reject) => {
        Taro.request(option).then(res => {
            resolve(res.data)
        }).catch(e => reject(e)).finally(() => {
            console.log(option.url + ' ==>本次请求结束');
        });
    });
}

export function getMiniAppConfig(url: string, data: any){
    let params: Taro.request.Option = {url, data, method: 'GET'};
    return baseRequest(params);
}
